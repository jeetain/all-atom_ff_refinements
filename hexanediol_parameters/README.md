# Hexanediol Force Field Parameters 
**Molecular insights into the effect of hexanediol on FUS phase separation**

Parameter sets for 1,6- and 2,5-hexanediol generated using ACPYPE ([GitHub](https://github.com/alanwilter/acpype)) 
with AMBER-compatible force fields. Validation includes density and molar volume calculations using TIP4P water model.

## Contents

### Parameter Files
| File               | Description                          |
|--------------------|--------------------------------------|
| `16HD.gro`         | 1,6-hexanediol structure (GROMACS)  |
| `16HD.itp`         | Topology for 1,6-hexanediol         |
| `posre_16HD.itp`   | Position restraints for 1,6-HD      |
| `25HD.gro`         | 2,5-hexanediol structure (GROMACS) |
| `25HD.itp`         | Topology for 2,5-hexanediol         |
| `posre_25HD.itp`   | Position restraints for 2,5-HD      |

## Installation

### Requirements
- GROMACS ≥2022
- AMBER Tools ≥22 (for parameter generation)
- Python ≥3.9 with ACPYPE ≥2023.1

### Parameter Integration
1. Insert molecules (.gro) to your system via gmx inster-molecules.

2. Include topology files in your system's `.top` file.
